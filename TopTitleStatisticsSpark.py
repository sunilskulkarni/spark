#!/usr/bin/env python3
import sys
from pyspark import SparkConf, SparkContext
import os

#/usr/local/Cellar/apache-spark/2.4.5/bin/spark-submit TopTitleStatisticsSpark.py partA partB



def wordCountFunc(line):
    values = line.split("\t")
    # mapFile = open("map", "a+")
    # mapFile.write('%s\n' % (values[1]))
    # mapFile.close()
    return int(values[1])

def wordSum2Func(line):
    values = line.split("\t")
    # mapFile = open("mapSum2", "a+")
    # mapFile.write('%s\n' % (values[1]))
    # mapFile.close()
    return int(values[1]) **2

def reduceSumFunc(a,b):
    # reduceFile = open("reduce", "a+")
    # reduceFile.write('%s\t%s\n' % (a, b))
    # reduceFile.close()
    return a + b

def reduceMinFunc(a,b):
    # reduceFile = open("reduceMin", "a+")
    # reduceFile.write('%s\t%s\n' % (a, b))
    # reduceFile.close()
    if (a < b):
        return a
    else:
        return b

def reduceMaxFunc(a,b):
    # reduceFile = open("reduceMax", "a+")
    # reduceFile.write('%s\t%s\n' % (a, b))
    # reduceFile.close()
    if (a > b):
        return a
    else:
        return b

def reduceSum2Func(a,b):
    # reduceFile = open("reduceSum2", "a+")
    # reduceFile.write('%s\t%s\n' % (a, b))
    # reduceFile.close()
    return a + b **2

conf = SparkConf().setMaster("local").setAppName("TopTitleStatistics")
conf.set("spark.driver.bindAddress", "127.0.0.1")
sc = SparkContext(conf = conf)

lines = sc.textFile(sys.argv[1],1)
wordcount = lines.map(wordCountFunc)
list_elements = wordcount.collect()
count = len(list_elements)
sum = wordcount.reduce(reduceSumFunc)
mean = round(sum/count)
min = wordcount.reduce(reduceMinFunc)
max = wordcount.reduce(reduceMaxFunc)

wordSum2count = lines.map(wordSum2Func)
sum2 = wordSum2count.reduce(reduceSumFunc)
var= round((sum2/count) - (sum/count)**2)

outputFile = open(sys.argv[2],"w")
outputFile.write('Mean\t%s\n' % mean)
outputFile.write('Sum\t%s\n' % sum)
outputFile.write('Min\t%s\n' % min)
outputFile.write('Max\t%s\n' % max)
outputFile.write('Var\t%s\n' % var)
outputFile.close()

sc.stop()

# 1020	1684
# 2704	893
# 3597	936
# 4533	1948
# 6481	1077
# 7558	771
# 8329	1065
# 9394	940
# 10334	800


# Mean	1113
# Sum	11134
# Min	771
# Max	1948
# Var	136010

# Mean	1113
# Sum	11134
# Min	771
# Max	1948
# Var	136010