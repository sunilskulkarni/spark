#!/usr/bin/env python3

'''Exectuion Command: spark-submit TitleCountSpark.py stopwords.txt delimiters.txt dataset/titles/ dataset/output'''
#https://weknowinc.com/blog/running-multiple-python-versions-mac-osx
# Veevas-MacBook-Pro-8:MP2Spark_py sunilkulkarni$ pyenv versions
#   system
#   3.6.9
#   3.6.9/envs/list
#   3.6.9/envs/python36
#   3.8.0
#   3.8.0/envs/python38
#   list
# * python36 (set by /Users/sunilkulkarni/.pyenv/version)
#   python38
# Veevas-MacBook-Pro-8:MP2Spark_py sunilkulkarni$
#pyenv global python36
#/usr/local/Cellar/apache-spark/2.4.5/bin/spark-submit TitleCountSpark.py stopwords.txt delimiters.txt dataset/titles/ partA

import sys
from pyspark import SparkConf, SparkContext
import re

stopWordsPath = sys.argv[1]
delimitersPath = sys.argv[2]

with open(stopWordsPath) as f:
	stopwords = f.read().splitlines()

delimiters = "[\\t,;.:?!\\-@\\[\\](){}_*/ ]+"


def linesToWordsFunc(line):
    words = [j for j in re.split(delimiters, line.lower().strip()) if j and j not in stopwords]
    return words

def wordsToPairsFunc(word):
    return (word, 1)

def reduceToCount(a, b):
    return (a + b)


conf = SparkConf().setMaster("local").setAppName("TitleCount")
conf.set("spark.driver.bindAddress", "127.0.0.1")
sc = SparkContext(conf = conf)


lines = sc.textFile(sys.argv[3],1)

words = lines.flatMap(linesToWordsFunc)
pairs = words.map(wordsToPairsFunc)
counts = pairs.reduceByKey(reduceToCount)


#outputFile = open(sys.argv[4],"w")

top_10 = sorted(counts.takeOrdered(10, key=lambda x: (-1 * x[1], x[0])))
output = sorted(top_10)

#print(output)


# for(word, count) in output:
#     print(word + ': ' + str(count))

# rdd = sc.parallelize(output)
# rdd.collect()
# rdd.saveAsTextFile(outputFile)

with open(sys.argv[4], 'w') as f:
    for(word, count) in output:
        f.write("%s\t%s\n" % (word, str(count)))

sc.stop()

# county	1020
# de	1684
# disambiguation	893
# john	936
# list	1948
# new	1077
# route	771
# school	1065
# state	940
# station	800
