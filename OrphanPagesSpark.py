#!/usr/bin/env python3
import sys
from pyspark import SparkConf, SparkContext
import os
from collections import defaultdict

#/usr/local/Cellar/apache-spark/2.4.5/bin/spark-submit OrphanPagesSpark.py dataset/links/links-x partC

def processVerteexRecord(record):
    #print(record)
    mapFile1 = open("FMap_rdd__vertex", "a+")
    mapFile1.write('%s\n' % (int(record)))
    mapFile1.close()

def processVisitedRecord(record):
    #print(record)
    mapFile1 = open("FMap_rdd__visited", "a+")
    mapFile1.write('%s\n' % (int(record)))
    mapFile1.close()

def processMapRecord(record):
    #print(record)
    mapFile1 = open("Map_rdd", "a+")
    mapFile1.write('%s\n' % (int(record)))
    mapFile1.close()

def flatmapKeyFunc(line):
    words = line.split(':')
    key = words[0]
    mapFile1 = open("FMap_key", "a+")
    mapFile1.write('%s\n' % ((key)))
    mapFile1.close()
    return key

def flatmapValueFunc(line):
    words = line.split(':')
    #key = words[0]
    value = words[1]
    values = value.split()
    # mapFile2 = open("FMap_values", "a+")
    # mapFile2.write('%s\n' % ((values)))
    # mapFile2.close()
    return values

def mapFunc(line):
    values = line.split(':')
    key = values[0]
    # mapFile = open("Map", "a+")
    # mapFile.write('%s\n' % (key))
    # mapFile.close()
    return key



conf = SparkConf().setMaster("local").setAppName("OrphanPages")
conf.set("spark.driver.bindAddress", "127.0.0.1")
sc = SparkContext(conf = conf)


lines = sc.textFile(sys.argv[1],1)

# wordcount = lines.map(mapFunc)

# keys = lines.flatMap(flatmapKeyFunc)
# keys.foreach(processVerteexRecord)

values = lines.flatMap(flatmapValueFunc)
#values.foreach(processVisitedRecord)


keys = lines.map(mapFunc)
#keys.foreach(processMapRecord)
# list_elements = keys.collect()
# count = len(list_elements)
# print('KEYS:%s' % count)

subtract = sorted(keys.subtract(values).collect())
#print('SUBTRACT:%s' % len(subtract) )
#print(type(subtract))
#print(subtract)

with open(sys.argv[2],"w") as f:
    for(word) in subtract:
        f.write("%s\n" % (word))

#wordcount = subtract.map(mapFunc)

# wordcount = keys.map(mapFunc)
# list_elements = wordcount.collect()
# count = len(list_elements)
# sum = wordcount.reduce(reduceSumFunc)

# with open(sys.argv[2],"w") as f:
#     for(word) in orphan:
#         f.write("%s\n" % (word))



sc.stop()

