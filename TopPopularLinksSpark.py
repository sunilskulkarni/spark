#!/usr/bin/env python
import sys
from pyspark import SparkConf, SparkContext

#/usr/local/Cellar/apache-spark/2.4.5/bin/spark-submit TopPopularLinksSpark.py dataset/links/ partD
def flatmapValueFunc(line):
    words = line.split(':')
    #key = words[0]
    value = words[1]
    values = value.split()
    # mapFile2 = open("FMap_values", "a+")
    # mapFile2.write('%s\n' % ((values)))
    # mapFile2.close()
    return values

def processVisitedRecord(record):
    print(record)
    # mapFile1 = open("FMap_rdd__visited", "a+")
    # mapFile1.write('%s\n' % (record))
    # mapFile1.close()

def mapIncrementVisited(visitedNode):
    return (visitedNode, 1)

def processMapRecord(record):
    print(record)
    # mapFile1 = open("Map_rdd", "a+")
    # mapFile1.write('%s\t%s\n' % (record[0], record[1]))
    # mapFile1.close()

def processReduceRecord(record):
    #print(record)
    mapFile1 = open("Reduce_rdd", "a+")
    mapFile1.write('%s\t%s\n' % (record[0], record[1]))
    mapFile1.close()

def reduceToCount(a, b):
    return (a + b)

conf = SparkConf().setMaster("local").setAppName("TopPopularLinks")
conf.set("spark.driver.bindAddress", "127.0.0.1")
sc = SparkContext(conf = conf)

lines = sc.textFile(sys.argv[1], 1) 
visited_nodes = lines.flatMap(flatmapValueFunc)
#values.foreach(processVisitedRecord)
pairs = visited_nodes.map(mapIncrementVisited)
# pairs.foreach(processMapRecord)
counts = pairs.reduceByKey(reduceToCount)
# counts.foreach(processReduceRecord)

top_10 = sorted(counts.takeOrdered(10, key=lambda x: (-1 * x[1], x[0])))
output = sorted(top_10)

#TODO
with open(sys.argv[2], 'w') as f:
    for(word, count) in output:
        f.write("%s\t%s\n" % (word, str(count)))

sc.stop()

