#!/usr/bin/env python3
import sys
from pyspark import SparkConf, SparkContext





def flatmapValueFunc(line):
    words = line.split(':')
    #key = words[0]
    value = words[1]
    values = value.split()
    # mapFile2 = open("FMap_values", "a+")
    # mapFile2.write('%s\n' % ((values)))
    # mapFile2.close()
    return values

def processVisitedRecord(record):
    print(record)
    # mapFile1 = open("FMap_rdd__visited", "a+")
    # mapFile1.write('%s\n' % (record))
    # mapFile1.close()

def mapIncrementVisited(visitedNode):
    # league = sc.textFile(sys.argv[2])
    # league_list = league.collect()
    # if(visitedNode in league_list):
        return (visitedNode, 1)

def processMapRecord(record):
    print(record)
    # mapFile1 = open("Map_rdd", "a+")
    # mapFile1.write('%s\t%s\n' % (record[0], record[1]))
    # mapFile1.close()

def processReduceRecord(record):
    #print(record)
    mapFile1 = open("output", "a+")
    mapFile1.write('%s\n' % (record))
    mapFile1.close()

def processJoinRecord(record):
    #print(record)
    mapFile1 = open("join", "a+")
    mapFile1.write('%s\n' % (record))
    mapFile1.close()

def reduceToCount(a, b):
    return (a + b)


conf = SparkConf().setMaster("local").setAppName("PopularityLeague")
conf.set("spark.driver.bindAddress", "127.0.0.1")
sc = SparkContext(conf = conf)



lines = sc.textFile(sys.argv[1], 1)

visited_nodes = lines.flatMap(flatmapValueFunc)

#values.foreach(processVisitedRecord)
pairs = visited_nodes.map(mapIncrementVisited)
# pairs.foreach(processMapRecord)
reduce_output = pairs.reduceByKey(reduceToCount)
#reduce_output.foreach(processReduceRecord)

league = sc.textFile(sys.argv[2], 1)

#https://stackoverflow.com/questions/26214112/filter-based-on-another-rdd-in-spark
employee = reduce_output.map(lambda e: (e[0],e[1]))
department = league.map(lambda d: (d,0))
result = employee.join(department).map(lambda e: (e[1][0], e[0])).collect()
result = sorted(result, key=str)
print('SIZE:%s' % len(result))


count, ordered_result = 0, {}
for (links, node) in result:
    print("%s\t%s" % (node, links))
    ordered_result[int(node)] = links



# for key in sorted(ordered_result):
#     print("{}: {}".format(key, ordered_result[key]))

# # print the list
# for element in list_elements:
#     print(element)


# top_10 = sorted(reduce_output.takeOrdered(10, key=lambda x: (-1 * x[1], x[0])))
# output = sorted(top_10)

# #TODO
# with open(sys.argv[3], 'w') as f:
#     for(word, count) in result:
#         f.write("%s\t%s\n" % (word, str(count)))

sc.stop()


